stages:
  - sanity check
  - bootstrapping
  - distributions
  - post children pipelines
  - ci-fairy
  - deploy
  - publish
  - test published images


#
# We want those to fail as early as possible, so we are using a plain fedora
# image, and there is no need to run `dnf update` as we only need to run
# one python script.
#

.pip_install:
  stage: sanity check
  image: python:alpine
  before_script:
    - apk add git

sanity check:
  extends: .pip_install
  script:
    - pip3 install --user PyYAML
    - pip3 install --user .
    - python3 ./src/generate_templates.py

    - git diff --exit-code && exit 0 || true

    - echo "some files were not generated through 'src/generate_templates.py' or
      have not been committed. Please edit the files under 'src', run
      'src/generate_templates.py' and then commit the result"
    - exit 1


check commits:
  extends: .pip_install
  script:
    - pip3 install .
    - ci-fairy check-commits --signed-off-by --junit-xml=results.xml
  except:
    - master@freedesktop/ci-templates
  variables:
    GIT_DEPTH: 100
    GIT_STRATEGY: clone
  artifacts:
    reports:
      junit: results.xml


pytest ci-fairy:
  extends: .pip_install
  script:
    - pip3 install pytest
    - pip3 install .
    - pytest --junitxml=results.xml
  artifacts:
    reports:
      junit: results.xml


flake8 ci-fairy:
  extends: .pip_install
  script:
    - pip3 install flake8
    # 501: line too long
    # 504: line break after binary operator
    # 741: ambiguous variable name
    - flake8 --ignore=W501,E501,W504,W741,E741


bootstrapping:
  stage: bootstrapping
  trigger:
    include:
      - local: '/.gitlab-ci/bootstrap-ci.yml'
    strategy: depend

{% for distribution in distribs %}
{{distribution}}:
  stage: distributions
  trigger:
    include:
      - local: '/.gitlab-ci/{{distribution}}-ci.yml'
    strategy: depend

{% endfor %}

#
# this is a dummy job that checks for the results of
# the children pipelines we just triggered
# when we fixed one of them by retrying the failed jobs,
# the rest of the pipeline is not retriggered.
# With this job, we can retry it and continue
# the pipeline.
#
check children pipelines:
  stage: post children pipelines
  image: alpine:latest
  before_script:
    - apk update
    - apk add jq curl
  script:
    - URL=https://gitlab.freedesktop.org/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/bridges
    - DATA=$(curl $URL)
    - echo $DATA | jq
    - CHILD_STATUSES=$(echo $DATA | jq -r '.[] | .downstream_pipeline.status' | sort -u | grep -v null)
    - echo $CHILD_STATUSES
    - '[ "$CHILD_STATUSES" == "success" ]'
  when: always

ci-fairy images:
  stage: ci-fairy
  trigger:
    include:
      - local: '/.gitlab-ci/ci-fairy-ci.yml'
    strategy: depend

.ci-fairy-tag:
  variables:
    FDO_DISTRIBUTION_TAG: {{ci_fairy.sha256sum(sha_source)}}

.ci-fairy-local-image:
  extends:
    - .ci-fairy-tag
  image: $CI_REGISTRY_IMAGE/ci-fairy:$FDO_DISTRIBUTION_TAG


check merge request:
  extends:
    - .ci-fairy-local-image
  stage: deploy
  script:
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=check-merge-request.xml
  artifacts:
    expire_in: 1 week
    when: on_failure
    paths:
      - check-merge-request.xml
    reports:
      junit: check-merge-request.xml
  variables:
    FDO_UPSTREAM_REPO: freedesktop/ci-templates
  # We allow this to fail because no MR may have been filed yet
  allow_failure: true


pages:
  extends: .pip_install
  stage: deploy
  script:
  - pip3 install sphinx sphinx-rtd-theme
  # Upstream bug in the HTML rendering for YAML nodes so let's used the fixed version
  # until this gets merged
  - pip3 install git+https://github.com/whot/sphinxcontrib-autoyaml.git@wip/fix-definition-rendering
  - sh -x doc/build-docs.sh
  - mv build public
  artifacts:
    paths:
    - public


#
# Template to publish to quay.io
# Requires: LOCAL_IMAGE_SPEC and DISTANT_IMAGE_SPEC to be set to the respective image paths
#
.publish.template:
  stage: publish
  image: $CI_REGISTRY_IMAGE/x86_64/buildah:{{bootstrap_tag}}
  script:
    - skopeo login --username "$QUAY_USER" --password "$(cat $QUAY_TOKEN)" {{ci_templates_registry}}
    - set -x
    - export LOCAL_IMAGE="$CI_REGISTRY_IMAGE/$LOCAL_IMAGE_SPEC"
    - export DISTANT_IMAGE="{{ci_templates_registry}}{{ci_templates_registry_path}}:$DISTANT_IMAGE_SPEC"

    # fetch the local and distant digest and layers to
    # ensure the image is not already on the remote

    # disable exit on failure
    - set +e

    # we store the labels and the sha of the layers
    # we can not rely on the digest: the Name is different
    # but the labels are set with the commit ID and the pipeline/job
    # ids, which should ensure a correct match
    - skopeo inspect docker://$LOCAL_IMAGE | jq '[.Labels, .Layers]' > local_sha

    # we store the same information from the distant registry
    - skopeo inspect docker://$DISTANT_IMAGE | jq '[.Labels, .Layers]' > distant_sha

    # reenable exit on failure
    - set -e

    # if the distant repo has an image, ensure we use the same
    # and finish the job if so
    - diff distant_sha local_sha && exit 0 || true

    # copy the original image into the distant registry
    - skopeo copy docker://$LOCAL_IMAGE docker://$DISTANT_IMAGE

    - set +x
    - skopeo logout {{ci_templates_registry}}


publish to {{ci_templates_registry}}:
  extends:
  - .publish.template
  parallel:
    matrix:
      - FDO_DISTRIBUTION_IMAGE:
          - buildah
        SUFFIX:
          - ''
        FDO_ARCHITECTURE:
          - x86_64
          - aarch64
        FDO_DISTRIBUTION_TAG:
          - '{{bootstrap_tag}}'
      - FDO_DISTRIBUTION_IMAGE:
          - qemu
          - qemu-mkosi
        SUFFIX:
          - '-base'
        FDO_ARCHITECTURE:
          - x86_64
        FDO_DISTRIBUTION_TAG:
          - '{{qemu_tag}}'
  variables:
    # local format,   e.g. x86_64/mkosi-base-12324
    # distant format, e.g. mkosi-base-x86_64-12324
    LOCAL_IMAGE_SPEC:   $FDO_ARCHITECTURE/${FDO_DISTRIBUTION_IMAGE}${SUFFIX}:${FDO_DISTRIBUTION_TAG}
    DISTANT_IMAGE_SPEC: $FDO_DISTRIBUTION_IMAGE${SUFFIX}-$FDO_ARCHITECTURE-${FDO_DISTRIBUTION_TAG}
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_PATH == "freedesktop/ci-templates"'

#
# Publish our ci-fairy image on quay.io
#
publish ci-fairy to {{ci_templates_registry}}:
  extends:
    - .ci-fairy-tag
    - .publish.template
  variables:
    LOCAL_IMAGE_SPEC:   ci-fairy:$FDO_DISTRIBUTION_TAG
    DISTANT_IMAGE_SPEC: ci-fairy-$FDO_DISTRIBUTION_TAG
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_PATH == "freedesktop/ci-templates"'

#
# Verify that all {{ci_templates_registry}} images directly referenced by our templates
# exist
#
test published images:
  image: $CI_REGISTRY_IMAGE/x86_64/buildah:{{bootstrap_tag}}
  stage: test published images
  script:
    {% for remote_image in remote_images %}
    - skopeo inspect docker://{{remote_image}}
    {% endfor %}
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_PATH == "freedesktop/ci-templates"'
